/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 11/6/19.
//

#include "ds_norbit_mb/norbitmb.h"
#include "norbitmb_private.h"
#include "endian.h"

#define STATUS_TIMEOUT 0.25
#define CMD_TIMEOUT 0.5

using namespace ds_norbit_mb;

NorbitMBDriver::NorbitMBDriver() : ds_base::DsProcess(),
d_ptr_(std::unique_ptr<NorbitMBDriverPrivate>(new NorbitMBDriverPrivate))
{
}

NorbitMBDriver::NorbitMBDriver(int argc, char* argv[], const std::string& name) : ds_base::DsProcess(argc, argv, name),
                                                                                  d_ptr_(std::unique_ptr<NorbitMBDriverPrivate>(new NorbitMBDriverPrivate))
{
}

NorbitMBDriver::~NorbitMBDriver() = default;

///*--------------------*///
///*  SETUP FUNCTIONS  *///
///*-------------------*///
void NorbitMBDriver::setupTimers() {
    ds_base::DsProcess::setupTimers();
    DS_D(NorbitMBDriver);
    auto nh = nodeHandle();
    const auto name = ros::this_node::getName();
    d->timer_ = nh.createTimer(ros::Duration(1.0), &NorbitMBDriver::timerCallback, this);
}

void NorbitMBDriver::setupParameters()
{
    ds_base::DsProcess::setupParameters();
    DS_D(NorbitMBDriver);
    auto nh = this->nodeHandle();
    const auto name = ros::this_node::getName();

    nh.param<std::string>("set_defaults", d->set_defaults_, "set_default");
    //nh.param<bool>("set_power", d->set_power_, false);
    nh.param<std::string>("set_trigger_mode", d->set_trigger_mode_, "4");
    nh.param<std::string>("set_frame_id", d->set_frame_id_, "norbit_link");
    //nh.getParam(name + "/" + "frame_id", d->set_frame_id_);
    nh.param<std::string>("set_mode", d->set_mode_, "1");
    nh.param<std::string>("set_time_source", d->set_time_source_, "0");
    nh.param<std::string>("set_sidescan_mode", d->set_sidescan_mode_, "1");
    nh.param<std::string>("set_flip", d->set_flip_, "1");
    nh.param<std::string>("set_gate_mode", d->set_gate_mode_, "0");
    nh.param<std::string>("set_gate_tilt", d->set_gate_tilt_, "0");
    nh.param<std::string>("set_range", d->set_range_, "0.5 10");
    nh.param<std::string>("set_range_gate_mode_2", d->set_range_gate_mode_2_, "0 10.0 0 10.0");
    nh.param<std::string>("set_direction", d->set_direction_, "0");
    nh.param<std::string>("set_opening_angle", d->set_opening_angle_, "90");
    nh.param<std::string>("set_resolution", d->set_resolution_, "256 128");
    nh.param<std::string>("set_tx_direction", d->set_tx_direction_, "0");
    nh.param<std::string>("set_tx_opening_angle", d->set_tx_opening_angle_, "140");
    nh.param<std::string>("set_tx_beam_number", d->set_tx_beam_number_, "1");
    nh.param<std::string>("set_mf_decimation", d->set_mf_decimation_, "1");
    nh.param<std::string>("set_tx", d->set_tx_, "400 80 5 500");
    nh.param<std::string>("set_tx_freq", d->set_tx_freq_, "400");
    nh.param<std::string>("set_tx_bw", d->set_tx_bw_, "80");
    nh.param<std::string>("set_tx_amp", d->set_tx_amp_, "5");
    nh.param<std::string>("set_tx_len", d->set_tx_len_, "500");
    nh.param<std::string>("set_rate", d->set_rate_, "-1");
    nh.param<std::string>("set_sound_velocity", d->set_sound_velocity_, "-1");
    nh.param<std::string>("set_ntp_server", d->set_ntp_server_, "10.0.1.100");
    nh.param<std::string>("set_autogate_preset", d->set_autogate_preset_, "0");
    nh.param<std::string>("set_resample", d->set_resample_, "2");

    d->length_ = ros::param::param<int>("~length", 5232);
    d->header_ = ros::param::param<std::string>("~header", "0xdeadbeef");
    d->max_clock_offset_ = ros::param::param<double>("~max_clock_offset", 0.5);

    d->get_power_ = ros::param::param<std::string>("~get_power", "set_power ");
    d->get_mode_ = ros::param::param<std::string>("~get_mode", "set_mode ");
    d->get_sidescan_mode_ = ros::param::param<std::string>("~get_sidescan_mode", "set_sidescan_mode ");
    d->get_flip_ = ros::param::param<std::string>("~get_flip", "set_flip ");
    d->get_gate_mode_ = ros::param::param<std::string>("~get_gate_mode", "set_gate_mode ");
    d->get_gate_tilt_ = ros::param::param<std::string>("~get_gate_tilt", "set_gate_tilt ");
    d->get_range_ = ros::param::param<std::string>("~get_range", "set_range ");
    d->get_range_gate_mode_2_ = ros::param::param<std::string>("~get_gate_tilt", "set_range ");
    d->get_direction_ = ros::param::param<std::string>("~get_direction", "set_direction ");
    d->get_opening_angle_ = ros::param::param<std::string>("~get_opening_angle", "set_opening_angle ");
    d->get_resolution_ = ros::param::param<std::string>("~get_resolution", "set_resolution ");
    d->get_tx_direction_ = ros::param::param<std::string>("~get_tx_direction", "set_tx_direction ");
    d->get_tx_opening_angle_ = ros::param::param<std::string>("~get_tx_opening_angle", "set_tx_opening_angle ");
    d->get_tx_beam_number_ = ros::param::param<std::string>("~get_tx_beam_number", "set_tx_beam_number ");
    d->get_mf_decimation_ = ros::param::param<std::string>("~get_mf_decimation", "set_mf_decimation ");
    d->get_tx_ = ros::param::param<std::string>("~get_tx", "set_tx ");
    d->get_rate_ = ros::param::param<std::string>("~get_rate", "set_rate ");
    d->get_trigger_mode_ = ros::param::param<std::string>("~get_trigger_mode", "set_trigger_mode ");
    d->get_sound_velocity_ = ros::param::param<std::string>("~get_sound_velocity", "sound_velocity ");
    d->get_time_source_ = ros::param::param<std::string>("~get_time_source", "set_time_source ");
    d->get_ntp_server_ = ros::param::param<std::string>("~get_ntp_server", "set_ntp_server ");
    d->get_autogate_preset_ = ros::param::param<std::string>("~get_autogate_preset", "set_autogate_preset ");
    d->get_resample_ = ros::param::param<std::string>("~get_resample", "set_resample ");

    ROS_INFO_STREAM("params set");

}

void NorbitMBDriver::setupConnections() {
    ds_base::DsProcess::setupConnections();
    DS_D(NorbitMBDriver);
    auto nh = nodeHandle();
    d->norbit_query_conn_ = addConnection("norbit_query_connection", boost::bind(&NorbitMBDriver::parseQueryCmdResp, this, _1));
    d->norbit_bathy_conn_ = addConnection("norbit_bathy_connection", boost::bind(&NorbitMBDriver::parseReceivedBytes, this, _1));
    d->iosm = addIoSM("statemachine", "norbit_mbsm");

    auto conn = boost::dynamic_pointer_cast<ds_asio::DsTcpClient>(d->norbit_bathy_conn_);
    if (conn) {
        int length = d->length_;
        ROS_WARN_STREAM("Length is: "<<length);
        //nh.param<int>(ros::this_node::getName() + "/" + name_ +"/length", length, 5232);
        std::string hexAscii = d->header_;
        ROS_WARN_STREAM("Header is: "<<hexAscii);
        //nh.param<std::string>(ros::this_node::getName() + "/" + name_ + "/header", hexAscii, "0xdeadbeef");
        std::vector<unsigned char> myHeader;
        for (size_t i=0;i<hexAscii.length(); i+=10)
        {
            std::string byteString = hexAscii.substr(i, 10);
            ROS_WARN_STREAM("byteString: "<<byteString);
            unsigned int myByte;
            sscanf(byteString.c_str(), "%X", &myByte);
            myHeader.push_back((unsigned char)myByte);
            ROS_WARN_STREAM("myByte: "<<myByte);
        }
        conn->set_matcher(match_header_length(myHeader, length));
        ROS_ERROR_STREAM("Overriding default matcher function");
    }
    ROS_INFO_STREAM("connections set");

    //sendNorbitCmd(d->set_defaults_);
    sendNorbitCmd(d->get_ntp_server_ + d->set_ntp_server_);
    sendNorbitCmd(d->get_trigger_mode_ + d->set_trigger_mode_);
    sendNorbitCmd(d->get_opening_angle_ + d->set_opening_angle_);
    sendNorbitCmd(d->get_resolution_ + d->set_resolution_);
    //Set pinging on
    d->set_power_ = std::to_string(1);
    sendNorbitCmd(d->get_power_ + d->set_power_);
    ROS_INFO_STREAM("sending default config");
}

void NorbitMBDriver::setupServices() {
    ds_base::DsProcess::setupServices();
    DS_D(NorbitMBDriver);
    auto nh = nodeHandle();
    const auto name = ros::this_node::getName();

    std::string query_srv = ros::param::param<std::string>("~query_service", "query_cmd");
    d->query_srv_ = nh.advertiseService<ds_multibeam_msgs::QueryCmd::Request, ds_multibeam_msgs::QueryCmd::Response>
            (name + "/" + query_srv, boost::bind(&NorbitMBDriver::queryCmd, this, _1, _2));
    std::string ping_srv = ros::param::param<std::string>("~ping_service", "ping_cmd");
    d->ping_srv_ = nh.advertiseService<ds_multibeam_msgs::PingCmd::Request, ds_multibeam_msgs::PingCmd::Response>
            (name + "/" + ping_srv, boost::bind(&NorbitMBDriver::pingCmd, this, _1, _2));
    std::string settings_srv = ros::param::param<std::string>("~settings_service", "settings_cmd");
    d->settings_srv_ = nh.advertiseService<ds_multibeam_msgs::SettingsCmd::Request, ds_multibeam_msgs::SettingsCmd::Response>
            (name + "/" + settings_srv, boost::bind(&NorbitMBDriver::settingsCmd, this, _1, _2));
    ROS_INFO_STREAM("services set");
}

void NorbitMBDriver::setupPublishers() {
    ds_base::DsProcess::setupPublishers();
    DS_D(NorbitMBDriver);
    // prepare our publishers
    auto nh = nodeHandle();
    const auto name = ros::this_node::getName();

    auto mbraw_topic = ros::param::param<std::string>("~mbraw_topic", "mbraw");
    d->mbraw_pub_ = nh.advertise<ds_multibeam_msgs::MultibeamRaw>(name+"/"+mbraw_topic, 10);

    auto norbitstatus_topic = ros::param::param<std::string>("~norbitstatus_topic", "norbitstatus");
    d->norbitmb_pub_ = nh.advertise<ds_multibeam_msgs::NorbitMB>(name+"/"+norbitstatus_topic, 10);
    ROS_INFO_STREAM("publishers set");

    auto pointcloud_topic = ros::param::param<std::string>("~pointcloud_topic", "pointcloud");
    d->pointcloud_pub_ = nh.advertise<sensor_msgs::PointCloud2>(name + "/" + pointcloud_topic, 1000);

    auto soundspeed_topic = ros::param::param<std::string>("~soundspeed_topic", "soundspeed");
    d->soundspeed_pub_ = nh.advertise<ds_sensor_msgs::SoundSpeed>(name + "/" + soundspeed_topic, 10);
}

void NorbitMBDriver::setupSubscriptions() {
    ds_base::DsProcess::setupSubscriptions();
    DS_D(NorbitMBDriver);
    auto nh = this->nodeHandle();

    // nodeHandle wraps the default node handle creation process.  For ds_asio to work properly, you have
    // to use these specially-configured node handles
    d->dr_srv_.reset(new dynamic_reconfigure::Server<ds_norbit_mb::NorbitMBConfig>(this->nodeHandle("~")));
    d->cb_ = boost::bind(&NorbitMBDriver::dynamicParamCallback, this, _1, _2);
    d->dr_srv_->setCallback(d->cb_);
    ROS_INFO_STREAM("dynamic parameter server set");

     }
void NorbitMBDriver::timerCallback(const ros::TimerEvent &event __attribute__((unused)))
{
    DS_D(NorbitMBDriver);
}
void NorbitMBDriver::dynamicParamCallback(ds_norbit_mb::NorbitMBConfig &config, uint32_t level)
{
    DS_D(NorbitMBDriver);
    auto nh = nodeHandle();

    ROS_ERROR("Reconfigure Request: %s %f %d %s %d %d %s %s %s %s %s %d %s %d %d %d %d %d %d %d %d %d %d",
             config.set_frame_id.c_str(), config.max_clock_offset, config.set_power,
             config.set_range.c_str(), config.set_direction, config.set_opening_angle,
             config.set_tx_freq.c_str(), config.set_tx_bw.c_str(), config.set_tx_amp.c_str(), 
             config.set_tx_len.c_str(), config.set_ntp_server.c_str(), config.set_rate, 
             config.set_sound_velocity.c_str(), config.set_mf_decimation, config.set_mode, 
             config.set_gate_mode, config.set_gate_tilt, config.set_sidescan_mode, config.set_trigger_mode, 
             config.set_time_source, config.set_autogate_preset, config.set_resample, config.set_default);

    d->set_frame_id_ = config.set_frame_id;
    d->max_clock_offset_ = config.max_clock_offset;

    if (d->power_cmd_ != config.set_power) {
        d->power_cmd_ = config.set_power;
        if (config.set_power == 1) {
            d->set_power_ = std::to_string(1);
        }
        else {
            d->set_power_ = std::to_string(0);
        }
        sendNorbitCmd(d->get_power_ + d->set_power_);
    }
    if (d->set_range_ != config.set_range) {
        d->set_range_ = config.set_range;
        sendNorbitCmd(d->get_range_ + d->set_range_);
    }
    if (d->set_direction_ != std::to_string(config.set_direction)) {
        d->set_direction_ = std::to_string(config.set_direction);
        sendNorbitCmd(d->get_direction_ + d->set_direction_);
    }
    if (d->set_opening_angle_ != std::to_string(config.set_opening_angle)) {
        d->set_opening_angle_ = std::to_string(config.set_opening_angle);
        sendNorbitCmd(d->get_opening_angle_ + d->set_opening_angle_);
    }
/*     if (d->set_tx_ != config.set_tx) {
        d->set_tx_ = config.set_tx;
        sendNorbitCmd(d->get_tx_ + d->set_tx_);
    } */
    if (d->set_tx_freq_ != config.set_tx_freq) {
        d->set_tx_freq_ = config.set_tx_freq;
        sendNorbitCmd(d->get_tx_ + d->set_tx_freq_ + " " + d->set_tx_bw_ + " " + d->set_tx_amp_ + " " + d->set_tx_len_);
    }
    if (d->set_tx_bw_ != config.set_tx_bw) {
        d->set_tx_bw_ = config.set_tx_bw;
        sendNorbitCmd(d->get_tx_ + d->set_tx_freq_ + " " + d->set_tx_bw_ + " " + d->set_tx_amp_ + " " + d->set_tx_len_);
    }
    if (d->set_tx_amp_ != config.set_tx_amp) {
        d->set_tx_amp_ = config.set_tx_amp;
        sendNorbitCmd(d->get_tx_ + d->set_tx_freq_ + " " + d->set_tx_bw_ + " " + d->set_tx_amp_ + " " + d->set_tx_len_);
    }
    if (d->set_tx_len_ != config.set_tx_len) {
        d->set_tx_len_ = config.set_tx_len;
        sendNorbitCmd(d->get_tx_ + d->set_tx_freq_ + " " + d->set_tx_bw_ + " " + d->set_tx_amp_ + " " + d->set_tx_len_);
    }
    if (d->set_ntp_server_ != config.set_ntp_server) {
        d->set_ntp_server_ = config.set_ntp_server;
        ROS_WARN_STREAM("NTP Server set to: "<<d->set_ntp_server_);
        sendNorbitCmd(d->get_ntp_server_ + d->set_ntp_server_);
    }
    if (d->set_rate_ != std::to_string(config.set_rate)) {
        d->set_rate_ = std::to_string(config.set_rate);
        sendNorbitCmd(d->get_rate_ + d->set_rate_);
    }
    if (d->set_sound_velocity_ != config.set_sound_velocity) {
        d->set_sound_velocity_ = config.set_sound_velocity;
        sendNorbitCmd(d->get_sound_velocity_ + d->set_sound_velocity_);
    }
    if (d->set_mf_decimation_ != std::to_string(config.set_mf_decimation)) {
        d->set_mf_decimation_ = std::to_string(config.set_mf_decimation);
        sendNorbitCmd(d->get_mf_decimation_ + d->set_mf_decimation_);
    }
    if (d->set_gate_mode_ != std::to_string(config.set_gate_mode)) {
        d->set_gate_mode_ = std::to_string(config.set_gate_mode);
        sendNorbitCmd(d->get_gate_mode_ + d->set_gate_mode_);
    }
    if (d->set_gate_tilt_ != std::to_string(config.set_gate_tilt)) {
        d->set_gate_tilt_ = std::to_string(config.set_gate_tilt);
        sendNorbitCmd(d->get_gate_tilt_ + d->set_gate_tilt_);
    }
    if (d->set_trigger_mode_ != std::to_string(config.set_trigger_mode)) {
        d->set_trigger_mode_ = std::to_string(config.set_trigger_mode);
        sendNorbitCmd(d->get_trigger_mode_ + d->set_trigger_mode_);
    }
    if (d->set_mode_ != std::to_string(config.set_mode)) {
        d->set_mode_ = std::to_string(config.set_mode);
        sendNorbitCmd(d->get_mode_ + d->set_mode_);
    }
    if (d->set_time_source_ != std::to_string(config.set_time_source)) {
        d->set_time_source_ = std::to_string(config.set_time_source);
        sendNorbitCmd(d->get_time_source_ + d->set_time_source_);
    }
    if (d->set_autogate_preset_ != std::to_string(config.set_autogate_preset)) {
        d->set_autogate_preset_ = std::to_string(config.set_autogate_preset);
        sendNorbitCmd(d->get_autogate_preset_ + d->set_autogate_preset_);
    }
    if (d->set_resample_ != std::to_string(config.set_resample)) {
        d->set_resample_ = std::to_string(config.set_resample);
        sendNorbitCmd(d->get_resample_ + d->set_resample_);
    }
    if (config.set_default) {
        sendNorbitCmd(d->set_defaults_);
    }
    //sendHeadConfig(); //Params have changed so resend config to head
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

// Parse messages from norbit multibeam via port 2210
void NorbitMBDriver::parseReceivedBytes(const ds_core_msgs::RawData& bytes) {
    DS_D(NorbitMBDriver);
    if (!parseHeader(bytes)) {
        ROS_ERROR_STREAM("NORBIT PARSE FAILED!");
    }
}

bool NorbitMBDriver::parseHeader(const ds_core_msgs::RawData& bytes) {
    DS_D(NorbitMBDriver);
    //ROS_ERROR_STREAM(bytes.data.data());
    auto data_size = bytes.data.size();
    auto hdr_size = sizeof(ds_norbit_mb::norbit_structs::header);
    //ROS_ERROR_STREAM("Bytes Data Size: "<<data_size<<" and Header Size: "<<hdr_size);
    if (data_size < hdr_size) {
        ROS_INFO_STREAM("Raw data ("<<data_size<<" bytes) less than minimum size: "<<hdr_size<<" bytes");
        return false;
    }

    const uint8_t* payload_bytes = bytes.data.data() + sizeof(ds_norbit_mb::norbit_structs::header);
    ds_norbit_mb::norbit_structs::header const* hdr = reinterpret_cast<ds_norbit_mb::norbit_structs::header const*>(bytes.data.data());
    size_t payload_size = hdr->data_size - sizeof(ds_norbit_mb::norbit_structs::header);

    if (data_size < hdr->data_size) {
      ROS_ERROR_STREAM("Bytes received: " <<data_size<<" < size of norbit bathy msg: " << hdr->data_size);
      return false;
    }

    boost::crc_32_type crc_result;
    crc_result.process_bytes(payload_bytes, payload_size);

    if (static_cast<int>(hdr->preamble) != static_cast<int>(0xdeadbeef)) {
        ROS_ERROR_STREAM("Wrong preamble received for bathy msg: " << std::hex<<hdr->preamble);
        return false;
    }

    if (static_cast<int>(hdr->crc) != static_cast<int>(crc_result.checksum())) {
        ROS_ERROR_STREAM("CRCs do not match: generated is: "<<crc_result.checksum()<<" and received crc is: " << hdr->crc);
        return false;
    }
    parseMessage(bytes);
    return true;
}

bool NorbitMBDriver::parseMessage(const ds_core_msgs::RawData& bytes) {
    DS_D(NorbitMBDriver);

    // Cast incoming data to structs
    auto processed_bytes = bytes.data.data() +sizeof(ds_norbit_mb::norbit_structs::header);
    auto *msg = reinterpret_cast<struct ds_norbit_mb::norbit_structs::status_msg const *>(processed_bytes);
    auto *beams = reinterpret_cast<struct ds_norbit_mb::norbit_structs::beam_msg const *>(processed_bytes
                                                                                         + sizeof(ds_norbit_mb::norbit_structs::status_msg));
    auto beam_msg_bytes = (bytes.data.data() + sizeof(ds_norbit_mb::norbit_structs::header) + sizeof(ds_norbit_mb::norbit_structs::status_msg));
    // Set headers for norbit and mbraw msgs
    d->norbit.header = bytes.header;
    d->mbraw.header = bytes.header;
    ros::Time stamp(msg->ping_time);
    d->norbit.header.stamp = stamp;
    d->norbit.header.frame_id = d->set_frame_id_;
    //ROS_ERROR_STREAM("Frame ID: "<<d->norbit.header.frame_id);
    d->mbraw.header.stamp = stamp;
    d->mbraw.header.frame_id = d->norbit.header.frame_id;
    d->norbit.ds_header = bytes.ds_header;
    d->mbraw.ds_header = bytes.ds_header;

    // Determine the authoritative timestamp for this message
    ros::Duration dt = d->norbit.ds_header.io_time - d->norbit.header.stamp;
    if (fabs(dt.toSec()) > d->max_clock_offset_) {
        //ROS_WARN_STREAM("Multibeam clock is: "<< d->norbit.header.stamp.toSec());
        ROS_WARN_STREAM("Multibeam clock differs from CPU clock by " <<dt.toSec() << " seconds (threshold: "
                        <<d->max_clock_offset_ <<"); using I/O times");
        d->norbit.header.stamp = d->norbit.ds_header.io_time;
        d->mbraw.header.stamp = d->norbit.ds_header.io_time;
    }

    //ROS_ERROR_STREAM("num beams: "<<msg->num_beams);
    //ROS_ERROR_STREAM("ping num: "<<msg->ping_num);

    //Do some helpful resizing for num_beams
    int num_beams = msg->num_beams;
    auto time_now = d->norbit.header.stamp.toSec();
    d->norbit.sample_num.resize(num_beams);
    d->norbit.beam_distance.resize(num_beams);
    d->norbit.angle.resize(num_beams);
    d->norbit.upper_gate.resize(num_beams);
    d->norbit.lower_gate.resize(num_beams);
    d->norbit.intensity.resize(num_beams);
    d->norbit.flags.resize(num_beams);
    d->norbit.quality_flag.resize(num_beams);
    d->norbit.quality_val.resize(num_beams);

    //MBRAW resizing
    d->mbraw.beamflag.resize(num_beams);
    d->mbraw.twowayTravelTime.resize(num_beams);
    d->mbraw.txDelay.resize(num_beams);
    d->mbraw.intensity.resize(num_beams);
    d->mbraw.angleAlongTrack.resize(num_beams);
    d->mbraw.angleAcrossTrack.resize(num_beams);
    d->mbraw.beamwidthAlongTrack.resize(num_beams);
    d->mbraw.beamwidthAcrossTrack.resize(num_beams);

    // Parse received message into struct
    d->norbit.snd_velocity = msg->snd_velocity;
    
    d->soundspeed_msg.speed = msg->snd_velocity;
    d->soundspeed_msg.raw_speed = d->soundspeed_msg.speed;
    d->mbraw.soundspeed = msg->snd_velocity;
    d->norbit.sample_rate = msg->sample_rate;
    d->norbit.num_beams = msg->num_beams;
    d->norbit.ping_num = msg->ping_num;
    d->norbit.ping_time = msg->ping_time;
    d->norbit.time_net = msg->time_net;
    d->norbit.ping_rate = msg->ping_rate;
    d->norbit.bathy_datatype = msg->bathy_datatype;

    for (size_t i=0; i < 8;i++) {
        d->norbit.beam_dist_mode[i] = static_cast<char>(msg->beam_dist_mode);
    }

    d->norbit.sonar_mode = msg->sonar_mode;
    d->norbit.tx_angle = msg->tx_angle;
    d->norbit.gain = msg->gain;
    d->norbit.tx_freq = msg->tx_freq * .001; //Hz to KHz
    d->norbit.tx_bw = msg->tx_bw * .001; //Hz to Khz
    d->norbit.tx_len = msg->tx_len;
    d->norbit.tx_voltage = msg->tx_voltage;
    d->norbit.swath_dir = msg->swath_dir;
    d->norbit.swath_open = msg->swath_open;
    d->norbit.gate_tilt = msg->gate_tilt;

    //Set some default values
    int num_good = 0;
    std::vector<float> ranges, depths;
    float min_range = 100;
    float max_range = 0;
    float min_depth = 100;
    float max_depth = 0;
    float center_angle = 100;
    float center_range = 0;
    float center_depth = 0;
    ranges.resize(num_beams);
    depths.resize(num_beams);

    //Parse data into struct and create array with beam info
    const size_t beam_msg_size = sizeof(ds_norbit_mb::norbit_structs::beam_msg);


    //Parse received beam info into struct
    for (size_t i=0; i < num_beams; i++) {
        d->mbraw.twowayTravelTime[i] = beams[i].sample_num / msg->sample_rate;
        d->norbit.sample_num[i] = beams[i].sample_num;
        d->norbit.beam_distance[i] = beams[i].sample_num * msg->snd_velocity/(2.0 * msg->sample_rate);
        d->norbit.angle[i] = beams[i].angle;
        d->mbraw.angleAlongTrack[i] = msg->tx_angle;
        d->mbraw.angleAcrossTrack[i] = beams[i].angle;
        d->norbit.upper_gate[i] = beams[i].upper_gate;
        d->norbit.lower_gate[i] = beams[i].lower_gate;
        d->norbit.intensity[i] = beams[i].intensity/msg->gain;
        d->mbraw.intensity[i] = d->norbit.intensity[i];
        d->norbit.flags[i] = beams[i].flags;
        d->norbit.quality_flag[i] = static_cast<int>(beams[i].quality_flag);
        d->norbit.quality_val[i] = static_cast<int>(beams[i].quality_val);
        d->mbraw.txDelay[i] = (0);
        d->mbraw.beamflag[i] = beams[i].quality_flag == 0 ? d->mbraw.BEAM_BAD_SONAR : d->mbraw.BEAM_OK;
        if (d->mbraw.beamflag[i] != d->mbraw.BEAM_BAD_SONAR) {
            num_good++;
        }
        ranges[i] = msg->snd_velocity * d->mbraw.twowayTravelTime[i] / 2.0;
        depths[i] = ranges[i] * cos(d->mbraw.angleAlongTrack[i])*cos(d->mbraw.angleAcrossTrack[i]);
        if (abs(d->mbraw.angleAcrossTrack[i]) < abs(center_angle)) {
            center_angle = d->mbraw.angleAcrossTrack[i];
            center_range = ranges[i];
            center_depth = depths[i];
        }
        min_range = (ranges[i] < min_range ? ranges[i] : min_range);
        max_range = (ranges[i] > max_range ? ranges[i] : max_range);
        min_depth = (depths[i] < min_depth ? depths[i] : min_depth);
        max_depth = (depths[i] > max_depth ? depths[i] : max_depth);
    }
    //Use beam info to set msg values
    d->norbit.percent_good = 100.0 * num_good / static_cast<float>(num_beams);
    d->norbit.min_depth = min_depth;
    d->norbit.max_depth = max_depth;
    d->norbit.center_depth = center_depth;
    d->norbit.min_range = min_range;
    d->norbit.max_range = max_range;
    d->norbit.center_range = center_range;

    sensor_msgs::PointCloud2Modifier modifier(d->cloud_msg);
    modifier.setPointCloud2Fields(4, "x", 1, sensor_msgs::PointField::FLOAT32, "y", 1,
            sensor_msgs::PointField::UINT32, "z", 1, sensor_msgs::PointField::FLOAT32,
            "intensity", 1, sensor_msgs::PointField::FLOAT32);
    modifier.resize(num_beams);

    d->cloud_msg.header = bytes.header;
    d->cloud_msg.header.stamp = d->norbit.header.stamp;
    d->cloud_msg.header.frame_id = d->norbit.header.frame_id;
    d->cloud_msg.height = 1;
    d->cloud_msg.width = num_beams;
    sensor_msgs::PointCloud2Iterator<float> iter_distance(d->cloud_msg, "x");
    sensor_msgs::PointCloud2Iterator<float> iter_acrossTrack(d->cloud_msg, "y");
    sensor_msgs::PointCloud2Iterator<float> iter_alongTrack(d->cloud_msg, "z");
    sensor_msgs::PointCloud2Iterator<float> iter_intensity(d->cloud_msg, "intensity");
    for (size_t i=0; i<num_beams;++i,++iter_acrossTrack, ++iter_distance, ++iter_alongTrack, ++iter_intensity) {
        *iter_acrossTrack = sin(d->norbit.angle[i]) * d->norbit.beam_distance[i];
        *iter_distance = cos(d->norbit.angle[i]) * d->norbit.beam_distance[i];
        *iter_alongTrack = 0;
        *iter_intensity = d->norbit.intensity[i];
    }

    d->mbraw_pub_.publish(d->mbraw);
    d->norbitmb_pub_.publish(d->norbit);
    d->pointcloud_pub_.publish(d->cloud_msg);
    d->soundspeed_pub_.publish(d->soundspeed_msg);
    return true;
}
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// Pick queries or ping, pwr, settings cmd to send to multibeam
bool NorbitMBDriver::queryCmd(ds_multibeam_msgs::QueryCmd::Request& req, ds_multibeam_msgs::QueryCmd::Response& res) {
    DS_D(NorbitMBDriver);
    res.command_sent = sendNorbitCmd(req.setting_name);
    return true;
}
bool NorbitMBDriver::pingCmd(ds_multibeam_msgs::PingCmd::Request& req, ds_multibeam_msgs::PingCmd::Response& res) {
    DS_D(NorbitMBDriver);
    switch(req.ping) {
        case ds_multibeam_msgs::PingCmd::Request::PING_START:
            //sendNorbitCmd("set_power 1");
            sendNorbitCmd(d->get_power_ + std::to_string(1));
            res.action = "commanded ping start";
            // Send Norbit Config
            break;
        case ds_multibeam_msgs::PingCmd::Request::PING_STOP:
            //sendNorbitCmd("set_power 0");
            sendNorbitCmd(d->get_power_ + std::to_string(0));
            res.action = "Commanded ping stop";
            break;
        default:
            ROS_WARN_STREAM("Ping command not recognized");
    }
    return true;
}

bool NorbitMBDriver::settingsCmd(ds_multibeam_msgs::SettingsCmd::Request& req, ds_multibeam_msgs::SettingsCmd::Response& res) {
    DS_D(NorbitMBDriver);
    res.command_sent = sendNorbitParam(req.setting_name, req.setting_value);
    //res.command_sent = "Commanded param";
    return true;
}

bool NorbitMBDriver::loadXmlCmd(ds_multibeam_msgs::LoadXmlCmd::Request& req, ds_multibeam_msgs::LoadXmlCmd::Response& res) {
    DS_D(NorbitMBDriver);
    res.command_sent = "commanded xml load";
    return true;
}

// Send commands to multibeam via port 2209
std::string NorbitMBDriver::sendNorbitCmd(std::string cmd) {
    DS_D(NorbitMBDriver);
    ROS_ERROR_STREAM("Sending: "<<cmd);
    std::string msg = cmd;
    auto head_cmd = ds_asio::IoCommand(cmd, CMD_TIMEOUT, false, boost::bind(&NorbitMBDriver::parseQueryCmdResp, this, _1));
    head_cmd.setDelayAfter(ros::Duration(CMD_TIMEOUT));
    d->iosm->addPreemptCommand(std::move(head_cmd));
    ros::Duration(1.0).sleep();
    return msg;
}

// Send params to multibeam via port 2209
std::string NorbitMBDriver::sendNorbitParam(std::string param_name, int param_value) {
    DS_D(NorbitMBDriver);
    ROS_ERROR_STREAM("Param is: "<<param_name<<" "<<param_value);
    std::string msg = param_name + " " + std::to_string(param_value);
    d->norbit_query_conn_->send(msg);
    return msg;
}
// Parse multibeam responses to commands sent directly to it
bool NorbitMBDriver::parseQueryCmdResp(const ds_core_msgs::RawData& bytes) {
    DS_D(NorbitMBDriver);
    auto msg = std::string{reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size() };
    if (msg[0] == 'W') {
        return false;
    }
    else {
        //ROS_ERROR_STREAM("msg received: "<<msg);
        auto buf_str = std::istringstream{ msg };
        std::vector<std::string> fields;

        while(buf_str.good() ) {
            std::string substr;
            getline(buf_str, substr, '\n');
            fields.push_back(substr);
        }
        for (size_t i=0; i<fields.size();i++) {
            ROS_INFO_STREAM("msg received: "<<fields[i]);
            i++;
        }
    }
    return true;
}
