/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 8/29/19.
//

#ifndef PROJECT_DS_NORBIT_MB_STRUCTS_H
#define PROJECT_DS_NORBIT_MB_STRUCTS_H

namespace ds_norbit_mb {
    namespace norbit_structs {
        //Total Size (in bytes): 5228

        struct header {
            //Size (in bytes): 24
            uint32_t preamble;
            uint32_t data_type;
            uint32_t data_size;
            uint32_t version;
            uint32_t reserved_1;
            uint32_t crc;
        } __attribute__((packed));

        struct status_msg {
            //Size (in bytes): 84
            float snd_velocity;
            float sample_rate;
            uint32_t num_beams;
            uint32_t ping_num;
            double ping_time;
            double time_net;
            float ping_rate;
            uint16_t bathy_datatype;
            uint8_t beam_dist_mode;
            uint8_t sonar_mode;
            uint16_t reserved_1[4];
            float tx_angle;
            float gain;
            float tx_freq;
            float tx_bw;
            float tx_len;
            float reserved_5;
            float tx_voltage;
            float swath_dir;
            float swath_open;
            float gate_tilt;
        } __attribute__((packed));

        struct beam_msg {
            //Size (in bytes): 5120
            uint32_t sample_num;
            float angle;
            uint16_t upper_gate;
            uint16_t lower_gate;
            float intensity;
            uint16_t flags;
            uint8_t quality_flag;
            uint8_t quality_val;
        } __attribute__((packed));

    } // norbit_structs
} //ds_norbit_mb
#endif //PROJECT_DS_NORBIT_MB_STRUCTS_H
