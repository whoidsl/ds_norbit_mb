/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 11/1/19.
//

#ifndef DS_NORBIT_MB_PRIVATE_H
#define DS_NORBIT_MB_PRIVATE_H

#include "ds_norbit_mb/norbitmb.h"

namespace ds_norbit_mb {
    struct NorbitMBDriverPrivate {

        // \brief Dynamic Reconfigure Server
        //TODO Make this a shared ptr

        boost::shared_ptr<dynamic_reconfigure::Server<ds_norbit_mb::NorbitMBConfig>> dr_srv_;
        //dynamic_reconfigure::Server<dr_srv_ptr> dr_srv;

        //auto ser_ptr = std::make_shared<dynamic_reconfigure::Server<ds_norbit_mb::DynReconf1Config>>(dr_srv_);

        //typedef boost::shared_ptr<ds_norbit_mb::DynReconf1Config> dr_srv_ptr_;
        //dynamic_reconfigure::Server<dr_srv_ptr_> dr_srv_;
        //dynamic_reconfigure::Server<dr_srv_ptr_>::CallbackType cb_;

        //dynamic_reconfigure::Server<ds_norbit_mb::DynReconf1Config> dr_srv_;
        dynamic_reconfigure::Server<ds_norbit_mb::NorbitMBConfig>::CallbackType cb_;

        /// \brief Loaded from param server
        ros::ServiceServer ping_srv_;
        ros::ServiceServer settings_srv_;
        ros::ServiceServer query_srv_;
        ros::ServiceServer xml_srv_;

        // \brief Publishers
        ros::Publisher mbraw_pub_;
        ros::Publisher norbitmb_pub_;
        ros::Publisher soundspeed_pub_;
        ros::Publisher pointcloud_pub_;

        // \brief Timers
        ros::Timer timer_;
        ros::Timer configTimer_;

        // \brief Messages
        ds_multibeam_msgs::NorbitMB norbit;
        ds_multibeam_msgs::MultibeamRaw mbraw;
        sensor_msgs::PointCloud2 cloud_msg;
        ds_sensor_msgs::SoundSpeed soundspeed_msg;

        //Params
        std::string header_;
        int length_;
        double max_clock_offset_;
        std::string set_frame_id_;
        bool power_cmd_;


        //SEE TABLE 1: Control commands of WBMS DFD Document for more info
        std::string set_defaults_;
        std::string set_mode_;
        std::string set_sidescan_mode_;
        std::string set_flip_;
        std::string set_gate_mode_;
        std::string set_gate_tilt_;
        std::string set_range_;
        std::string set_range_gate_mode_2_;
        std::string set_direction_;
        std::string set_opening_angle_;
        std::string set_resolution_;
        std::string set_tx_direction_;
        std::string set_tx_opening_angle_;
        std::string set_tx_beam_number_;
        std::string set_mf_decimation_;
        std::string set_tx_;
        std::string set_tx_freq_;
        std::string set_tx_bw_;
        std::string set_tx_amp_;
        std::string set_tx_len_;
        std::string set_rate_;
        std::string set_trigger_mode_;
        std::string set_sound_velocity_;
        std::string set_time_source_;
        std::string set_ntp_server_;
        std::string set_autogate_preset_;
        std::string set_power_;
        std::string set_resample_;

        //Queryable commands
        std::string get_power_;
        std::string get_mode_;
        std::string get_sidescan_mode_;
        std::string get_gate_mode_;
        std::string get_gate_tilt_;
        std::string get_range_;
        std::string get_direction_;
        std::string get_opening_angle_;
        std::string get_resolution_;
        std::string get_tx_direction_;
        std::string get_tx_opening_angle_;
        std::string get_tx_beam_number_;
        std::string get_mf_decimation_;
        std::string get_tx_;
        std::string get_tx_freq_;
        std::string get_tx_bw_;
        std::string get_tx_amp_;
        std::string get_tx_len_;
        std::string get_rate_;
        std::string get_RESERVED_rate_;
        std::string get_trigger_mode_;
        std::string get_time_source_;
        std::string get_ntp_server_;
        std::string get_sound_velocity_;
        std::string get_autogate_preset_;
        std::string get_resample_;
        std::string get_flip_;
        std::string get_range_gate_mode_2_;

        /// \brief I/O State Machine Connection
        boost::shared_ptr<ds_asio::IoSM> iosm;

        // Connections
        /// \brief TCP Connection for command and control
        boost::shared_ptr<ds_asio::DsConnection> norbit_query_conn_;
        /// \brief TCP Connection for bathymetric data
        boost::shared_ptr<ds_asio::DsConnection> norbit_bathy_conn_;
    };
}
#endif //DS_NORBIT_MB_PRIVATE_H
