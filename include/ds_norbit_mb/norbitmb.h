/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 08/19/19.
//

#ifndef PROJECT_NORBIT_MB_H
#define PROJECT_NORBIT_MB_H

#include <ds_base/ds_process.h>
#include "ds_multibeam_msgs/MultibeamRaw.h"
#include "ds_multibeam_msgs/NorbitMB.h"
#include "ds_multibeam_msgs/QueryCmd.h"
#include "ds_multibeam_msgs/PingCmd.h"
#include "ds_multibeam_msgs/PwrCmd.h"
#include "ds_multibeam_msgs/SettingsCmd.h"
#include "ds_multibeam_msgs/LoadXmlCmd.h"
#include "ds_sensor_msgs/SoundSpeed.h"
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include "../../src/ds_norbit_mb/norbit_structs.h"
#include <string>
#include <memory>
#include <boost/crc.hpp>
#include <ds_base/util.h>

#include <dynamic_reconfigure/server.h>
#include <ds_norbit_mb/NorbitMBConfig.h>

namespace ds_norbit_mb
{
struct NorbitMBDriverPrivate;

class NorbitMBDriver : public ds_base::DsProcess
{
    DS_DECLARE_PRIVATE(NorbitMBDriver)

public:
  explicit NorbitMBDriver();
  NorbitMBDriver(int argc, char* argv[], const std::string& name);
  ~NorbitMBDriver() override;
  DS_DISABLE_COPY(NorbitMBDriver)
  bool parseMessage(const ds_core_msgs::RawData& bytes);
  bool parseHeader(const ds_core_msgs::RawData& bytes);
  void dynamicParamCallback(ds_norbit_mb::NorbitMBConfig &config, uint32_t level);

protected:
  /// \function overrides from DsProcess
  void setupParameters() override;
  void setupConnections() override;
  void setupServices() override;
  void setupPublishers() override;
  void setupSubscriptions() override;
  void setupTimers() override;

  void timerCallback(const ros::TimerEvent &event);
  void setupDynamicReconfigure(const ros::TimerEvent &event);
  void sendHeadConfig();
  void queryConfigParams();
  void parseReceivedBytes(const ds_core_msgs::RawData& bytes);
  bool parseQueryCmdResp(const ds_core_msgs::RawData& bytes);

  bool queryCmd(ds_multibeam_msgs::QueryCmd::Request& req, ds_multibeam_msgs::QueryCmd::Response& res);
  bool pingCmd(ds_multibeam_msgs::PingCmd::Request& req, ds_multibeam_msgs::PingCmd::Response& res);
  bool pwrCmd(const ds_multibeam_msgs::PwrCmd::Request& req, const ds_multibeam_msgs::PwrCmd::Response& res);
  bool settingsCmd(ds_multibeam_msgs::SettingsCmd::Request& req, ds_multibeam_msgs::SettingsCmd::Response& res);
  bool loadXmlCmd(ds_multibeam_msgs::LoadXmlCmd::Request& req, ds_multibeam_msgs::LoadXmlCmd::Response& res);

  std::string sendNorbitCmd(std::string cmd);
  std::string sendNorbitParam(std::string param_name, int param_value);


private:
    std::unique_ptr<NorbitMBDriverPrivate> d_ptr_;


};
} //namespace ds_norbit_mb

#endif  // PROJECT_NORBIT_MB_H
